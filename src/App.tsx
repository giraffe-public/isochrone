import { useCallback, useEffect, useState } from 'react'

// @ts-ignore
import union from '@turf/union';
import Form, { Profile, getIso } from './Form'
import { useGiraffeState } from '@gi-nx/iframe-sdk-react';
import { rpc, giraffeState } from '@gi-nx/iframe-sdk';
// @ts-ignore
import type { FeatureCollection, Feature, Point } from 'geojson';
// @ts-ignore
import { throttle } from 'lodash';


const layerName = 'isochrone';
const updateMap = throttle(async (points: Feature<Point>[], minutes: string, profile: Profile) => {
  const fcs = await Promise.all(points.map(f => {
    const [lon, lat] = f.geometry.coordinates;
    return getIso([lon, lat], profile, +minutes);

  }));
  let combined: FeatureCollection;
  if (fcs.length === 0) {
    combined = {
      type: "FeatureCollection",
      features: []
    }
  } else {
    combined = fcs[0].features?.[0];
    fcs.slice(1).forEach(fc => {
      combined = union(combined, fc.features?.[0]);
    })
  }
  console.log("combined", combined);

  rpc.invoke('updateTempLayerGeoJSON', [layerName, combined]);
}, 300, { leading: true, trailing: true, maxWait: 300 });


function App() {
  const bakedSections = useGiraffeState('bakedSections');
  const [profile, setProfile] = useState<Profile>('cycling');
  const [minutes, setMinutes] = useState('10');

  const isoPoints = bakedSections.features.filter((f: Feature<Point>) => f.geometry.type === 'Point' && f.properties?.isochrone);

  useEffect(() => {
    updateMap(isoPoints, minutes, profile);
  }, [bakedSections, minutes, profile]);


  const addPoint = useCallback(() => {
    rpc.invoke('createRawSection', [{type: "Feature", geometry: {type: "Point", coordinates: giraffeState.get('mapView').center }, properties: {
      isochrone: true,
      "usage": "Pin",
      "marker": {
        "kind": "text",
        "value": {
          "scale": 0.25,
          "shape": "square",
          "backgroundColor": "#E5F2FD",
          "borderColor": "#041178",
          "shapeSize": 40,
          "borderWidth": 3,
          "text": "isochrone source",
          "textSize": 30,
          "onTop": true,
          "renderMode": "screen",
          "stick": false,
          "stickHeight": 0,
          "anchor": "bottom"
        }
      },
    }}]);
  }, []) 

  return (
    <div style={{padding: '20px'}}>
      <h1 style={{fontSize: '24px'}}> Isochrone </h1>
      {(isoPoints.length === 0) && <>
        <br/>
        <p>Please add a point to the map with property <code>isochrone: true</code>.</p>
        <br/>
        <p style={{textDecoration: 'underline'}} onClick={addPoint}> OR click here to add one </p>
      </>}
      {isoPoints.length > 0 && <Form minutes={minutes} setMinutes={setMinutes} profile={profile} setProfile={setProfile} />}
    </div>
  )
}

export default App
