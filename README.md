# DEPRECATED: all SDK examples have moved to [the example repo](https://gitlab.com/giraffe-public/sdk-examples)

# An Isochrone app for Giraffe

This app ports the [Mapbox example](https://docs.mapbox.com/help/tutorials/get-started-isochrone-api/) to run in the Giraffe SDK.

It is built from React + Vite and deployed on Netflify following [this tutorial](https://docs.netlify.com/integrations/frameworks/vite/).

You can add this app to a Giraffe project by searching 'isochrone' in the app menu (+ on right).
If you run this locally and make changes, you can see the results by setting a project to look at a local URL instead: ☰ main menu -> Advanced -> JSON editor -> select 'isochrone' -> edit `private.url`.

### Setup

Create an app, I called it `isochrone`
```
npm create vite@latest
```

Install dependencies
```
yarn
yarn add @gi-nx/iframe-sdk-react
yarn add -D netlify-cli
```

Set the env variable
```
export VITE_MAPBOX_TOKEN=<get one from https://www.mapbox.com/>
```

Dev with 
```
yarn run dev
```

Build and deploy the app
```
yarn build
yarn run netlify deploy --prod --dir dist
```
